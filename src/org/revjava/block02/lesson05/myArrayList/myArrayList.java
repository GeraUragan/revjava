package org.revjava.block02.lesson05.myArrayList;
import java.util.Iterator;
/**
 * Created by agentfsb on 14.02.17.
 */
public class myArrayList <HH> implements Iterable<HH> {

    private Object[] array = new Object[3];
    private int quantity = 0;


    public void add(HH Item) {
        if (quantity == array.length - 1) {
            resize(array.length * 2);
        }
        array[quantity++] = Item;
    }


    public void remove(int index) {
        for (int i = index; i < quantity; i++) {
            array[i] = array[i + 1];
        }
        array[quantity] = null;
        quantity--;
        resize(quantity);
    }


    public HH get(int index) {
        return (HH) array[index];
    }


    public void replace(int indexFrom, int indexTo) {
        if (indexTo > quantity) {
            resize(indexTo * 2);
        }
        array[indexTo] = array[indexFrom];
        remove(indexFrom);
    }


    private void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, quantity);
        array = newArray;
    }


    public Iterator<HH> iterator() {
        return new Iterator<HH>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < quantity && array[currentIndex] != null;
            }

            @Override
            public HH next() {
                return (HH) array[currentIndex++];
            }

        };
    }
}





/*
* 1 2 3 4
* 1   3 4
* 1 2(3) 3(4)
* 1 2 3 4
*
* 2 1
*
*
* */
