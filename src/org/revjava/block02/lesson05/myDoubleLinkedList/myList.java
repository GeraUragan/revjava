package org.revjava.block02.lesson05.myDoubleLinkedList;

/**
 * Created by agentfsb on 19.02.17.
 */
public class myList {

    private Object data;
    private myList next;
    private myList previous;

    public Object getData(){
        return data;
    }

    public void setData (Object data){
        this.data = data;
    }

    public myList getNext(){
        return next;
    }

    public void setNext ( myList next){
        this.next = next;
    }

    public myList getPrevious(){
        return previous;
    }

    public void setPrevious ( myList previous){
        this.previous = previous;
    }
}