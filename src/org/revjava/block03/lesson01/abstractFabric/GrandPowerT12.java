package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 10.02.17.
 */
public class GrandPowerT12 extends Pistol {

    private static int cost = 125000;
    private static boolean auto = false;

    @Override
    public String getDescription() {
        return "Пистолет GrandPowerT12 стоимость:" + cost + " автоматический огонь:" + auto;
    }
}
