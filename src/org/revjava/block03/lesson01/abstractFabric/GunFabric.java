package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 09.02.17.
 */

//abstract fabric
public interface GunFabric {

    public Pistol createPistol();

    public Shotgun createShotgun();

}
