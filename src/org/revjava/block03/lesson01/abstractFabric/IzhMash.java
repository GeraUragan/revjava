package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 09.02.17.
 */

//concrete fabric
public class IzhMash implements GunFabric {

    @Override
    public Pistol createPistol() {
        return new Yarigin();
    }

    @Override
    public Shotgun createShotgun() {
        return new Saiga12();
    }
}
