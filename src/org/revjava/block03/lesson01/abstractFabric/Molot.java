package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 10.02.17.
 */

//concrete fabric
public class Molot implements GunFabric {

    @Override
    public Pistol createPistol() {
        return new GrandPowerT12();
    }

    @Override
    public Shotgun createShotgun() {
        return new Vepr12();
    }
}
