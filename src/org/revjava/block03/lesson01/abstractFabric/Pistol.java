package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 09.02.17.
 */

//concrete product

public abstract class Pistol {

    private int cost;
    private boolean auto;

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean getAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public abstract String getDescription();

}
