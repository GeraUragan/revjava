package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 09.02.17.
 */

//concrete product

public abstract class Shotgun {

    private int cost;
    private int caliber;

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getСaliber() {
        return caliber;
    }

    public void setСaliber(int caliber) {
        this.caliber = caliber;
    }

    public abstract String getDescription();

}
