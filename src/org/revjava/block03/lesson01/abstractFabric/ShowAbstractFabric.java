package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 09.02.17.
 */
/*
*                                                                                       -> product()
*                                                        -> concrete product(Saiga12)   -> product()
*                           -> concrete fabric(IshMash)                                 -> product()
*                                                        -> concrete product(Yarigin)   -> product()
* abstract fabric(GunFabric)                                                            -> product()
*                                                        -> concrete product(Vepr12)    -> product()
*                           -> concrete fabric(Molot)                                   -> product()
*                                                        -> concrete product(GP12)      -> product()
*/

public class ShowAbstractFabric {
    public static void main (String [] args) {

        GunFabric gunFabric = new IzhMash();
        System.out.println("ИжМаш: ");
        showProduction(gunFabric);

        System.out.println();

        gunFabric = new Molot();
        System.out.println("МолотАрмс:");
        showProduction(gunFabric);

    }

    public static void showProduction(GunFabric gunFabric) {

        Pistol pistol = gunFabric.createPistol();
        System.out.println(pistol.getDescription());

        Shotgun shotgun = gunFabric.createShotgun();
        System.out.println(shotgun.getDescription());

    }
}

