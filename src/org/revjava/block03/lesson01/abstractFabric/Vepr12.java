package org.revjava.block03.lesson01.abstractFabric;

/**
 * Created by agentfsb on 10.02.17.
 */
public class Vepr12 extends Shotgun {
    private static final int cost = 42000;
    private static final int caliber = 12;

    @Override
    public String getDescription() {
        return "Вепрь 12 стоимость :" + cost + " калибр:" + caliber;
    }
}
