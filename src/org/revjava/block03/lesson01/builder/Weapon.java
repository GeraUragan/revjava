package org.revjava.block03.lesson01.builder;

/**
 * Created by agentfsb on 12.02.17.
 */

public class Weapon {

    public final int cost;
    public final boolean auto;
    public final String cal;


    public static class Builder {


        private final int cost;
        private final String cal;


        private boolean auto = false;

        public Builder (int cost, String cal){
            this.cost = cost;
            this.cal = cal;
        }

        public Builder auto(boolean vol){
            auto = vol;
            return this;
        }

        public Weapon build(){
            return new Weapon(this);
        }
    }

        private Weapon(Builder builder){
            cost = builder.cost;
            auto = builder.auto;
            cal = builder.cal;
        }

}






