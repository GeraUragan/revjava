package org.revjava.block03.lesson01.builder;

/**
 * Created by agentfsb on 12.02.17.
 */
public class main {

    public static void main(String [] args){
        Weapon glock = new Weapon.
                Builder (2000, "9x19")
                .auto(true)
                .build();
        System.out.println(glock.cal);
    }
}
