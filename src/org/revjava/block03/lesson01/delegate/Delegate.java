package org.revjava.block03.lesson01.delegate;

/**
 * Created by agentfsb on 09.02.17.
 */
public class Delegate {

    public static void  main(String[] args){

        Brodyaga maga = new Brodyaga();
        maga.polzi();

    }
}

class Pityh implements PituhAction{

    public void polzi(){
        System.out.print("pituh pod shkonkoi");
    }

}

interface PituhAction{
    void polzi();
}

class Brodyaga{

    PituhAction loh = new Pityh();
    void polzi(){
        loh.polzi();
    }
}