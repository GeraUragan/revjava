package org.revjava.block03.lesson01.observerListener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
/**
 * Created by agentfsb on 07.02.17.
 */

class InputForm extends JFrame {
    JTextField input = new JTextField(10);
    private Observable myObservable = new Observable() {

        //фиксируем изменения, извещаем всех наблюдателей об ихменении родительского класса

        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg); //??
        }
    };

    //добавляем наблюдателя
    public void addObserver(OutputForm o) {

        myObservable.addObserver(o);

    }

    InputForm() throws IOException {

        //создаем слушателя
        input.addActionListener(new ActionListener() {
            //фиксируем вводимый текст
            public void actionPerformed(ActionEvent e) {
                myObservable.notifyObservers(input.getText());
            }
        });

        setSize(500, 500);
        setTitle("Наблюдаемая форма");
        add(input);
        setVisible(true);

    }


}

