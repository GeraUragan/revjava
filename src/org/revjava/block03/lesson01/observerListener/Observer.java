package org.revjava.block03.lesson01.observerListener;

import java.util.Observable;

/**
 * Created by agentfsb on 07.02.17.
 */

// интерфейс наблюдения

public interface Observer {

    void update(Observable o, Object arg);

}
