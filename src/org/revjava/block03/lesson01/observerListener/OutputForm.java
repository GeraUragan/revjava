package org.revjava.block03.lesson01.observerListener;

import javax.swing.*;
import java.util.Observable;

/**
 * Created by agentfsb on 07.02.17.
 */
//создаем форму-наблюдателя

public class OutputForm extends JFrame implements Observer, java.util.Observer {

    {
        setSize(500, 500);
        setTitle("Наблюдатель");
        setVisible(true);
    }


    //создаем диалоговое окно вывода сообщения

    @Override
    public void update(Observable o, Object arg) {
        JOptionPane.showMessageDialog(null  , arg);
    }




}
