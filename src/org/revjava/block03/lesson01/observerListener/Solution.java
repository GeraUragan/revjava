package org.revjava.block03.lesson01.observerListener;

import java.io.IOException;

/**
 * Created by agentfsb on 08.02.17.
 */

//создаем формы и заставляем одну наблюдать за другой

public class Solution {

    public static void main(String[] args) throws IOException {

        InputForm inputForm = new InputForm();
        OutputForm outputForm = new OutputForm();
        inputForm.addObserver(outputForm);

    }

}
