package org.revjava.block03.lesson01.singletone;

/**
 * Created by agentfsb on 06.02.17.
 */
class Singletone {

    private static Singletone singletone ;
    private Singletone() {

    }

    public static Singletone getSingletone () {

        if (singletone == null){
            singletone = new Singletone();
        }
        return singletone;

    }

}
